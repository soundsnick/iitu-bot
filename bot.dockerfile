FROM golang:alpine

WORKDIR /srv/iitu-bot

ADD . .

COPY go.mod .
COPY go.sum .

RUN go mod download

ENTRYPOINT go build -o iitu-bot  && ./iitu-bot