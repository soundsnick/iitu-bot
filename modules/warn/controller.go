package warn

import (
	"fmt"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/soundsnick/iitu-bot/core/types"
	"gitlab.com/soundsnick/iitu-bot/warnsstore"
)

// Controller for contact module
func Controller(c *types.Context) {
	warnsstore.WarnUser(c.Update.Message.Chat.UserName)
	warnedUser := c.Update.Message.ReplyToMessage.From
	warnCount := warnsstore.GetWarnCountOfUser(c.Update.Message.Chat.UserName)
	if warnCount >= 2 {
		c.Bot.KickChatMember(
			tgbotapi.KickChatMemberConfig{
				UntilDate: 1000000,
				ChatMemberConfig: tgbotapi.ChatMemberConfig{
					ChatID: c.Update.Message.Chat.ID,
					UserID: warnedUser.ID,
				},
			},
		)
		*c.Message = tgbotapi.NewMessage(c.Update.Message.Chat.ID, fmt.Sprintf("@%v получил предупреждение (3/3) и был забанен.", warnedUser.UserName))
	} else {
		warnsstore.WarnUser(warnedUser.UserName)
		*c.Message = tgbotapi.NewMessage(c.Update.Message.Chat.ID, fmt.Sprintf("@%v получил предупреждение (%v/3)", warnedUser.UserName, warnCount+1))
	}
	c.Message.ReplyToMessageID = c.Update.Message.MessageID
	c.Bot.Send(c.Message)
}
