package main

import (
	"log"
	"os"
	"strconv"

	"github.com/cornelk/hashmap"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/soundsnick/iitu-bot/core/helpers"
	"gitlab.com/soundsnick/iitu-bot/core/types"
	"gitlab.com/soundsnick/iitu-bot/modules/faq"
	"gitlab.com/soundsnick/iitu-bot/modules/warn"
)

func main() {
	log.Print("Debug mode: ", os.Getenv("TELEGRAM_BOT_DEBUG"))

	// __INIT__
	Commands := helpers.GetCommands()
	Store := hashmap.HashMap{}

	bot, err := tgbotapi.NewBotAPI(os.Getenv("TELEGRAM_BOT_TOKEN"))
	if err != nil {
		log.Panic(err)
	}

	bot.Debug, _ = strconv.ParseBool(os.Getenv("TELEGRAM_BOT_DEBUG"))

	log.Printf("Authorized on account %s", bot.Self.UserName)
	log.Print("Started listener")

	// Create context
	context := types.Context{
		Bot:      bot,
		Store:    &Store,
		Commands: &Commands,
	}

	// Listen for updates
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		admins, err := bot.GetChatAdministrators(update.Message.Chat.ChatConfig())
		if update.Message == nil || (update.Message.Chat.Type != "supergroup" && update.Message.Chat.Type != "group") || err != nil { // ignore any non-Message and non-group messages Updates
			continue
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		// Add Message responser and Update to context
		context.Message = &tgbotapi.MessageConfig{}
		context.Update = &update

		// Check if user has state
		command := update.Message.Text

		if command == Commands.Warn {
			var admin *tgbotapi.ChatMember
			for i := 0; i < len(admins); i++ {
				log.Print(admins[i].User.UserName, update.Message.From.UserName, admins[i].User.UserName == update.Message.From.UserName)

				if admins[i].User.UserName == update.Message.From.UserName {
					admin = &admins[i]
				}
			}
			if admin == nil {
				continue
			}
			go warn.Controller(&context)
		} else if command == Commands.FAQ {
			go faq.Controller(&context)
		} else {
			continue
		}
	}
}
