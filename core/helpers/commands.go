package helpers

import (
	"gitlab.com/soundsnick/iitu-bot/core/types"
)

// GetCommands returns commands
func GetCommands() types.Commands {
	return types.Commands{
		Start: "/start",
		Warn:  "/warn",
		FAQ:   "/faq",
	}
}
