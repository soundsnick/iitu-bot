package warnsstore

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

const store = "warnsstore/warns-store.json"

// GetWarnCountOfUser returns user language
func GetWarnCountOfUser(username string) int {
	content, err := ioutil.ReadFile(store)
	if err != nil {
		panic(err)
	}
	// Convert []byte to string and print to screen
	texts := string(content)
	var textsDeserialized map[string]int
	json.Unmarshal([]byte(texts), &textsDeserialized)
	if textsDeserialized[username] > 0 {
		return textsDeserialized[username]
	}
	return 0
}

// WarnUser sets user language
func WarnUser(username string) error {
	content, err := ioutil.ReadFile(store)
	if err != nil {
		panic(err)
	}
	// Convert []byte to string and print to screen
	texts := string(content)
	var textsDeserialized map[string]int
	json.Unmarshal([]byte(texts), &textsDeserialized)
	if val, ok := textsDeserialized[username]; ok {
		textsDeserialized[username] = val + 1
	} else {
		textsDeserialized[username] = 0
	}

	newStore, err := json.Marshal(textsDeserialized)
	if err != nil {
		return fmt.Errorf("error")
	}
	ioutil.WriteFile(store, newStore, 0644)
	return nil
}
