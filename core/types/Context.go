package types

import (
	"github.com/cornelk/hashmap"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

// Context returns context for controllers
type Context struct {
	Bot      *tgbotapi.BotAPI
	Update   *tgbotapi.Update
	Message  *tgbotapi.MessageConfig
	Store    *hashmap.HashMap
	Commands *Commands
}
