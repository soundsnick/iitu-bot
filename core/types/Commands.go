package types

// Commands type for commands
type Commands struct {
	Start string
	Warn  string
	FAQ   string
}
