package faq

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/soundsnick/iitu-bot/core/types"
)

// Controller for contact module
func Controller(c *types.Context) {
	*c.Message = tgbotapi.NewMessage(c.Update.Message.Chat.ID, "*ПРАВИЛА ГРУППЫ*:\n\n_Вопросы:_\nВОПРОСЫ ДОЛЖНЫ ФОРМИРОВАТЬСЯ ПО ПРАВИЛАМ nometa.xyz\n\n_Поведенческие нормы:_\n1. Группа аполитична, но можно говорить о политологии.\n2. Любой признак неадекватности карается предупреждением, (3 предупреждения - бан).\n3. Неадекватный представитель другого университета банится без предупреждений.\n\n_Реклама:_\n1. Любой вид рекламы не относящаяся к деятельности в университете, строго обговаривается с @askaroviiich.\n\n_Вакансии:_ \n1. Вакансия должна иметь четкое описание стека и требований.\n2. Вакансия должна иметь вилку зар.платы.\n3. Вакансия должна иметь доказательство о реальности. (как пример: ссылка на HH.kz)\n4. Вакансия не должна содержать в себе ссылку с формой, где кандидат заполняет свои данные. Мы за privacy. \nСПИСОК ПРАВИЛ БУДЕТ ДОПОЛНЯТЬСЯ ПО МЕРЕ ВОЗНИКНОВЕНИЯ СЛУЧАЕВ НАРУШЕНИЯ")
	c.Message.ReplyToMessageID = c.Update.Message.MessageID
	c.Message.ParseMode = "markdown"
	c.Bot.Send(c.Message)
}
